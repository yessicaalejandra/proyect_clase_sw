﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class personaController : Controller
    {
        private Model1Container db = new Model1Container();

        //
        // GET: /persona/

        public ActionResult Index()
        {
            return View(db.personaSet.ToList());
        }

        //
        // GET: /persona/Details/5

        public ActionResult Details(int id = 0)
        {
            persona persona = db.personaSet.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // GET: /persona/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /persona/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(persona persona)
        {
            if (ModelState.IsValid)
            {
                db.personaSet.Add(persona);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(persona);
        }

        //
        // GET: /persona/Edit/5

        public ActionResult Edit(int id = 0)
        {
            persona persona = db.personaSet.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // POST: /persona/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(persona);
        }

        //
        // GET: /persona/Delete/5

        public ActionResult Delete(int id = 0)
        {
            persona persona = db.personaSet.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        //
        // POST: /persona/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            persona persona = db.personaSet.Find(id);
            db.personaSet.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}