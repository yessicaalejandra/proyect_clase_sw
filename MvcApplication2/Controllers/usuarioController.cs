﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class usuarioController : Controller
    {
        private Model1Container db = new Model1Container();

        //
        // GET: /usuario/

        public ActionResult Index()
        {
            var usuarioset = db.usuarioSet.Include(u => u.persona);
            return View(usuarioset.ToList());
        }

        //
        // GET: /usuario/Details/5

        public ActionResult Details(int id = 0)
        {
            usuario usuario = db.usuarioSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // GET: /usuario/Create

        public ActionResult Create()
        {
            ViewBag.personaId = new SelectList(db.personaSet, "Id", "nombre");
            return View();
        }

        //
        // POST: /usuario/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.usuarioSet.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.personaId = new SelectList(db.personaSet, "Id", "nombre", usuario.personaId);
            return View(usuario);
        }

        //
        // GET: /usuario/Edit/5

        public ActionResult Edit(int id = 0)
        {
            usuario usuario = db.usuarioSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.personaId = new SelectList(db.personaSet, "Id", "nombre", usuario.personaId);
            return View(usuario);
        }

        //
        // POST: /usuario/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.personaId = new SelectList(db.personaSet, "Id", "nombre", usuario.personaId);
            return View(usuario);
        }

        //
        // GET: /usuario/Delete/5

        public ActionResult Delete(int id = 0)
        {
            usuario usuario = db.usuarioSet.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        //
        // POST: /usuario/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            usuario usuario = db.usuarioSet.Find(id);
            db.usuarioSet.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}