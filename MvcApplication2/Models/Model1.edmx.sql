
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/15/2015 08:19:00
-- Generated from EDMX file: d:\visual studio 2013\Projects\MvcApplication2\MvcApplication2\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [bd_proyecto];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'personaSet'
CREATE TABLE [dbo].[personaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [apellido] nvarchar(max)  NOT NULL,
    [correo] nvarchar(max)  NOT NULL,
    [Entity2_Id] int  NOT NULL
);
GO

-- Creating table 'Entity2Set'
CREATE TABLE [dbo].[Entity2Set] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre_usuario] nvarchar(max)  NOT NULL,
    [contraseña] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'personaSet'
ALTER TABLE [dbo].[personaSet]
ADD CONSTRAINT [PK_personaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Entity2Set'
ALTER TABLE [dbo].[Entity2Set]
ADD CONSTRAINT [PK_Entity2Set]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Entity2_Id] in table 'personaSet'
ALTER TABLE [dbo].[personaSet]
ADD CONSTRAINT [FK_personaEntity2]
    FOREIGN KEY ([Entity2_Id])
    REFERENCES [dbo].[Entity2Set]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_personaEntity2'
CREATE INDEX [IX_FK_personaEntity2]
ON [dbo].[personaSet]
    ([Entity2_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------